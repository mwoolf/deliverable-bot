package output

import (
  "strings"
)

func MarkdownOutput() string {
  outputStrings := make([]string, 2)
  outputStrings[0] = milestoneHeader("13.8")
  outputStrings[1] = issueRows()
  return strings.Join(outputStrings, "")
}

func milestoneHeader(milestoneName string) string  {
  header := `
## Milestone 13.8

| Issue      | Category |
| ----------- | ----------- |
`
  return header
}

func issueRows() string {
  issueRows := make([]string, len(GraphqlResponse.Project.Issues.Nodes))
  for i := 0; i < len(GraphqlResponse.Project.Issues.Nodes); i++ {
    issueRows[i] = outputForIssue(GraphqlResponse.Project.Issues.Nodes[i])
  }
  return strings.Join(issueRows, "")
}

func outputForIssue(issue issue) string {
  s := []string{"| [", issue.Title, "](", issue.WebUrl, ") | Category Name goes here |\n"}
  return strings.Join(s, "")
}

var GraphqlResponse response

type issue struct {
  ID string
  WebUrl string
  Title string
  Labels struct {
    Nodes []label
  }
}

type label struct {
  Title string
}

type response struct {
  Project struct {
    Name string
    Issues struct {
      Nodes []issue
    }
  }
}