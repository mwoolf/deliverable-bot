package postissue

import (
  "context"
  "fmt"
  "github.com/machinebox/graphql"
)

func Execute(description string) {
  fmt.Println(description)
	graphqlClient := graphql.NewClient("https://gitlab.com/api/graphql")
	mutation := fmt.Sprintf(`mutation {
 updateIssue(input: { projectPath: "gitlab-org/manage/compliance/general",
   iid: "16",
   description: """%s
"""}) {
   issue {
     updatedAt
   }
 }
}
`, description)
	graphqlRequest := graphql.NewRequest(mutation)
	graphqlRequest.Header.Add("Authorization", "Bearer 6ZVh8DwhVzNaE-qFJWdh")
	if err := graphqlClient.Run(context.Background(), graphqlRequest, nil); err != nil {
		panic(err)
	}
}
