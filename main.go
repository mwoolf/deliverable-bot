package main

import (
	"context"
	"deliverable-bot/internal/output"
	"deliverable-bot/internal/postissue"
	"fmt"
	"github.com/machinebox/graphql"
)

func main() {
	graphqlClient := graphql.NewClient("https://gitlab.com/api/graphql")
	graphqlRequest := graphql.NewRequest(`
{
  project(fullPath: "gitlab-org/gitlab") {
    issues(labelName: ["group::compliance", "feature"],
           milestoneTitle: "13.8",
           state: closed) {
      ...issues
    }
  }
}

fragment issues on IssueConnection {
  nodes {
    id
    title
    webUrl
    labels {
      nodes {
        title
      }
    }
    assignees {
      nodes {
        name
      }
    }
    milestone {
      title
    }
  }
}
    `)

	if err := graphqlClient.Run(context.Background(), graphqlRequest, &output.GraphqlResponse); err != nil {
		panic(err)
	}
	markdownOutput := output.MarkdownOutput()

	fmt.Println(markdownOutput)

	postissue.Execute(markdownOutput)

}
